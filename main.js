let navbarRow = document.querySelector('.navbarRow');
let TheRedSpecial = document.querySelector('.TheRedSpecial'); 
let logoAnimation = document.querySelector('#logoAnimation > *');
function checkWindowSize(){
  if (window.innerWidth < 576){
    navbarRow.classList.remove('tripleHorizBorder');
    navbarRow.style.height = '';
    TheRedSpecial.style.width = '100%';
  } else if ((window.innerWidth >= 576)&&(window.innerWidth < 768)){
    navbarRow.classList.remove('tripleHorizBorder');
    navbarRow.style.height = '50px';
    TheRedSpecial.style.width = '80%';
  } else if ((window.innerWidth >= 768)&&(window.innerWidth < 992)){
    navbarRow.classList.add('tripleHorizBorder');
    navbarRow.style.height = '50px';
    TheRedSpecial.style.width = '60%';
  } else if ((window.innerWidth >= 992)&&(window.innerWidth < 1200)){
    navbarRow.classList.add('tripleHorizBorder');
    navbarRow.style.height = '50px';
    TheRedSpecial.style.width = '40%';
  } else if (window.innerWidth >= 1200){
    navbarRow.classList.add('tripleHorizBorder');
    navbarRow.style.height = '50px';
    TheRedSpecial.style.width = '20%';
  }
}
checkWindowSize();
window.addEventListener('resize', () => {
    checkWindowSize();
})

function handleIntersection1(entries1){
  entries1.map(entry => {
    if(entry.isIntersecting){
      logoAnimation.classList.add('fadeIN');
    }
  });
}
let observerlogoAnimation = new IntersectionObserver(handleIntersection1);
observerlogoAnimation.observe(logoAnimation);

let slides = document.querySelectorAll('.mySlides');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function changeCard(a){
    let randomIndex2 = getRandomInt(0, (slides.length-1));
    slides[a].innerHTML = `
    <div class="card fadeThatCard" style="max-width: 100%;">
      <div class="row g-0">
        <img src="/img/London${randomIndex2}.jpg" class="img-fluid rounded" alt="...">              
      </div>
    </div>
  `;
}

setInterval(() => {
  let randomIndex = getRandomInt(0, (slides.length-1));
  changeCard(randomIndex);
}, 5000);